from dash import Dash
from dash import dcc
from dash import html
from dash.html.Title import Title
import pandas as pd
import numpy as np
from dash.dependencies import Input, Output

data = pd.read_csv('avocado.csv')
# data = data.query('type=="conventional" and region=="Albany"')


data['Date'] = pd.to_datetime(data['Date'], format='%Y-%m-%d')
data.sort_values('Date', inplace=True)

external_stylesheets = [
  {
    'href': 'https://fonts.googleapis.com/css2?',
    'family': 'Lato:wght@400;700&display=swap',
    'rel': 'stylesheet'
  }
]

app = Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

app.title = 'Avocado Analytics'

# app = Dash(__name__)
app.layout = html.Div(
  children=[
    html.Div(
      children=[
        html.P(children="🥑", className="header-emoji"),
        html.H1(
            children='Avocado Analytics',
            className="header-title"
          ), 
        html.P(
          children='Analise avocado prices in the US from 2015 to 2018',
          className="header-description"
          ),
      ],
      className='header',
    ),
    html.Div(
      children=[
        
        # Region selector
        html.Div(
          children=[
            html.Div(children='Region', className='menu-title'),
            dcc.Dropdown(
              id='region-filter',
              options=[
                {'label': region, 'value': region}
                for region in np.sort(data.region.unique())
              ],
              value='Albany',
              clearable=False,
              className='dropdown'
            ),
          ],
        ),
        
        # Type selector
        html.Div(
          children=[
            html.Div(children='Type', className='menu-title'),
            dcc.Dropdown(
              id='type-filter',
              options=[
                {'label': type, 'value': type}
                for type in np.sort(data.type.unique())
              ],
              value='organic',
              clearable=False,
              searchable=False,
              className='dropdown'
            ),
          ],
        ),

        # Date range selector
        html.Div(
          children=[
            html.Div(children='Date range', className='menu-title'),
            dcc.DatePickerRange(
              id='date-range-filter',
              min_date_allowed=data.Date.min().date(),
              max_date_allowed=data.Date.max().date(),
              start_date=data.Date.min().date(),
              end_date=data.Date.max().date(),
            ),
          ],
        ),

      ],
      className='filter-container',
    ),
    html.Div(
      children=[
        html.Div(
          children=dcc.Graph(
            id='price-chart',
            config={'displayModeBar': False},
          ),
          className='card',
        ),
        html.Div(
          children=dcc.Graph(
            id='volume-chart',
            config={'displayModeBar': False},
          ),
          className='card',
        ),
      ],
      className='wrapper',
    ),
  ]
)

@app.callback(
  [
    Output('price-chart', 'figure'),
    Output('volume-chart', 'figure'),
  ],
  [
    Input('region-filter', 'value'),
    Input('type-filter', 'value'),
    Input('date-range-filter', 'start_date'),
    Input('date-range-filter', 'end_date'),
  ],
)
def update_charts(region, type, start_date, end_date):
  mask = (
    (data.region == region) & (data.type == type)
    & (data.Date >= start_date) & (data.Date <= end_date) 
  )

  filtered_data = data.loc[mask, :]

  price_chart_figure = {
    'data': [
      {
        'x': filtered_data['Date'], 
        'y': filtered_data ['AveragePrice'], 
        'type': 'lines',
        'hovertemplate': '$%{y:.2f}<extra></extra>',
      },
    ],
    'layout': {
      'title': {
          'text': 'Average Price of Avocado',
          'x': 0.05,
          'xanchor': 'left',
        },
      'xaxis': {'fixedrange': True},
      'yaxis': {'fixedrange': True, 'tickprefix': '$'},
      'colorway': ['#17B897'],
    }
  }

  volume_chart_figure = {
    'data': [
      {
        'x': filtered_data['Date'], 
        'y': filtered_data['Total Volume'],
        'type': 'lines', 
      }
    ],
    'layout': {
        'title': {
          'text': 'Avocados sold',
          'x': 0.05,
          'xanchor': 'left',
        },
      'xaxis': {'fixedrange': True},
      'yaxis': {'fixedrange': True},
      'colorway': ['#E12D39'],
    },
  }

  return price_chart_figure, volume_chart_figure

if __name__ == '__main__':
  app.run_server(debug=True)